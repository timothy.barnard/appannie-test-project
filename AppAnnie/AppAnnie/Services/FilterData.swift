//
//  FilterData.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import Foundation
import Combine

/// Data structure to store the filtered values between views
struct FilterData {
    /// Constants values (not idea but ok for POC)
    let productId: Int = 846723902
    let accountId: Int = 250888
    var startDate: Date?
    var endDate: Date?
    var groupByCountry: Bool = false
    var groupByDate: Bool = false
    
    var startDateString: String {
        return startDate?.toMediumDateSyleString ?? "--"
    }
    
    var endDateString: String {
        return endDate?.toMediumDateSyleString ?? "--"
    }
    
    /// Resets the false to default states
    mutating func resets() {
        self.startDate = nil
        self.endDate = nil
        self.groupByDate = false
        self.groupByCountry = false
    }
}
