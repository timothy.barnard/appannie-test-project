//
//  DateExtensionTests.swift
//  AppAnnieTests
//
//  Created by Timothy Barnard on 12/09/2021.
//

import XCTest
@testable import AppAnnie

class DateExtensionTests: XCTestCase {

    func testDateToMediumStyleString() {
        let startDate1: Date = .create(year: 2020, month: 1, day: 1)
        XCTAssertEqual(startDate1.toMediumDateSyleString, "Jan 1, 2020")
        
        let startDate2: Date = .create(year: 2021, month: 15, day: 15)
        XCTAssertEqual(startDate2.toMediumDateSyleString, "Mar 15, 2022")
        
        let startDate3: Date = .create(year: 2025, month: 10, day: 10)
        XCTAssertEqual(startDate3.toMediumDateSyleString, "Oct 10, 2025")
    }

}
