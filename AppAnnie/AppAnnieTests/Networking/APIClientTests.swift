//
//  APIClientTests.swift
//  AppAnnieTests
//
//  Created by Timothy Barnard on 11/09/2021.
//

import XCTest
import Combine
@testable import AppAnnie

class APIClientTests: XCTestCase {
    
    var urlSession: URLSession!
    
    override func setUpWithError() throws {
        //create a session config that does not cache
        let configuration: URLSessionConfiguration = .ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]
        //create the session now with our MockURLProtocol
        urlSession = URLSession(configuration: configuration)
    }
    
    struct EmptyMockData: Decodable {}

    func testUnauthorisedErrorResponse() {
        let urlResponse = HTTPURLResponse(url: URL(string: "https://google.com")!, statusCode: 401, httpVersion: nil, headerFields: nil)!
        
        MockURLProtocol.requestHandler = { request in
            return (urlResponse, Data([0, 1, 0, 1]))
        }
        let apiClient = APIClient(self.urlSession, decoder: JSONDecoder())
        let publisher: AnyPublisher<EmptyMockData, APIFailureReason> = apiClient.makeRequest(api: .sales(accountId: 123, projectId: 123))
        
        let (data, failure) = wait(for: publisher)
        XCTAssertEqual(failure, .unauthorized)
        XCTAssertNil(data)
    }
    
    func testForbiddenErrorResponse() {
        let urlResponse = HTTPURLResponse(url: URL(string: "https://google.com")!, statusCode: 403, httpVersion: nil, headerFields: nil)!
        
        MockURLProtocol.requestHandler = { request in
            return (urlResponse, Data([0, 1, 0, 1]))
        }
        let apiClient = APIClient(self.urlSession, decoder: JSONDecoder())
        let publisher: AnyPublisher<EmptyMockData, APIFailureReason> = apiClient.makeRequest(api: .sales(accountId: 123, projectId: 123))
        
        let (data, failure) = wait(for: publisher)
        XCTAssertEqual(failure, .forbidden)
        XCTAssertNil(data)
    }
    
    func testDecodingFailedResponse() {
        let urlResponse = HTTPURLResponse(url: URL(string: "https://google.com")!, statusCode: 201, httpVersion: nil, headerFields: nil)!

        MockURLProtocol.requestHandler = { request in
            return (urlResponse, Data([0, 1, 0, 1]))
        }
        let apiClient = APIClient(self.urlSession, decoder: JSONDecoder())
        let publisher: AnyPublisher<EmptyMockData, APIFailureReason> = apiClient.makeRequest(api: .sales(accountId: 123, projectId: 123))
        let (data, failure) = wait(for: publisher)
        XCTAssertEqual(failure, .decodingFailed)
        XCTAssertNil(data)
    }
    
    func testSuccessResponse() {
        struct MockData: Decodable {
            let name: String
        }
        
        let jsonData = """
        {
            "name": "George"
        }
        """.data(using: .utf8)
        
        let urlResponse = HTTPURLResponse(url: URL(string: "https://google.com")!, statusCode: 201, httpVersion: nil, headerFields: nil)!

        MockURLProtocol.requestHandler = { request in
            return (urlResponse, jsonData!)
        }
        let apiClient = APIClient(self.urlSession, decoder: JSONDecoder())
        let publisher: AnyPublisher<MockData, APIFailureReason> = apiClient.makeRequest(api: .sales(accountId: 123, projectId: 123))
        let (data, failure) = wait(for: publisher)
        XCTAssertNil(failure)
        XCTAssertEqual(data?.name, "George")
    }

}

fileprivate extension XCTestCase {
    
    /// Helper method to wait for the publisher to be finished
    /// - Parameter publisher: `Publisher`
    /// - Returns: `(T.Output?, T.Failure?)`
    func wait<T: Publisher>(for publisher: T) -> (T.Output?, T.Failure?) {
        let waitForResponse = expectation(description: "wait for response")
        var failureReason: T.Failure?
        var output: T.Output?
        let anyCancellable = publisher
            .receive(on: DispatchQueue.main)
            .sink { result in
                switch result {
                case .failure(let reason):
                    failureReason = reason
                case .finished: break
                }
                waitForResponse.fulfill()
            } receiveValue: { value in
                output = value
            }
        waitForExpectations(timeout: 2)
        anyCancellable.cancel()
        return (output, failureReason)
    }
}
