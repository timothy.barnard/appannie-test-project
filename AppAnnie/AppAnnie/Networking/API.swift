//
//  API.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import Foundation


/**
 API enum for storing available endpoints
 */
enum API {
    
    /**
     The breakdown allows you to specify by which
     dimensions the results should be broken down.
     */
    enum BreakDown: String {
        case country
        case date
        case countryAndDay = "date+country"
        
        /// The breakdown built into query item
        var queryItem: URLQueryItem {
           .init(name: "break_down", value: rawValue)
        }
    }
    
    
    /// Wrapper around the dates filter
    struct DateFilter {
        let start: Date
        let end: Date
        
        /// Local date formatter with the correct format for the API
        private var dateFormatter: DateFormatter = {
            var formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()
        
        init(start: Date, end: Date) {
            self.start = start
            self.end = end
        }
        
        /// The dates built into query items
        var queryItems: [URLQueryItem] {
            [
                .init(
                    name: "start_date",
                    value: dateFormatter.string(from: start)),
                .init(
                    name: "end_date",
                    value: dateFormatter.string(from: end)),
            ]
        }
    }
    
    /// Get all sales for the given project
    case sales(accountId: Int, projectId: Int,
               breakDown: BreakDown? = nil,
                dateFilter: DateFilter? = nil)
    
    case countries
    
    /// URL host
    private var host: String {
        "api.appannie.com"
    }
    
    /// Path for a given API call
    private var path: String {
        switch self {
        case .sales(let accountId, let projectId, _ , _):
            return "/v1.3/accounts/\(accountId)/products/\(projectId)/sales"
        case .countries:
            return "/v1.3/meta/countries"
        }
    }
    
    /// All query items if available depending on a given API call
    private var urlQueryItems: [URLQueryItem]? {
        switch self {
        case .sales(_, _, let breakDown, let dateFilter):
            var queryItems: [URLQueryItem] = []
            if let queryItem = breakDown?.queryItem {
                queryItems.append(queryItem)
            }
            if let dateQueryItems = dateFilter?.queryItems {
                queryItems.append(contentsOf: dateQueryItems)
            }
            return queryItems
        case .countries:
            return nil
        }
    }
    
    /// The URL already built with host, endpoint and query items
    private var url: URL? {
        var urlComponents: URLComponents = .init()
        urlComponents.host = host
        urlComponents.scheme = "https"
        urlComponents.path = path
        urlComponents.queryItems = urlQueryItems
        return urlComponents.url
    }
    
    /// Return the API token from the Info.plist using the xcconfig file
    private var apiToken: String {
        /**
         Default to empty string to keep things simple. API will throw a 400 if empty.
         */
        Bundle.main.object(forInfoDictionaryKey: "API_KEY") as? String ?? ""
    }
    
    /// HTTP headers containing the relevant values including API key
    private var httpHeaders: [String: String] {
        [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(apiToken)"
        ]
    }
    
    /// The built request for the given API call
    var urlRequest: URLRequest? {
        guard let url = self.url else {return nil}
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = httpHeaders
        return request
    }
}
