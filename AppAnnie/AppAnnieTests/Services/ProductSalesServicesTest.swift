//
//  PoductSalesViewModelTests.swift
//  AppAnnieTests
//
//  Created by Timothy Barnard on 11/09/2021.
//

import XCTest
@testable import AppAnnie

class ProductSalesViewModelTests: XCTestCase {

    func testDataResponse() {
        let expectedData: [ProductSales] = [
            .init(downloads: 1, revenue: "1", currency: "$", country: "Netherlands", date: "2021-12-12"),
            .init(downloads: 2, revenue: "1", currency: "$", country: "American", date: "2021-12-13"),
        ]
        let request: MockRequest<[ProductSales]> = MockRequest(.success(expectedData))
        let expectation = expectation(description: "wait for request to finish")
        let anyCancellable = request
            .makeRequest(api: .sales(accountId: 123, projectId: 123))
            .receive(on: DispatchQueue.main)
            .sink { result in
                switch result {
                case .failure:
                    XCTFail("Failure was not expect")
                case .finished: break
                }
                expectation.fulfill()
            } receiveValue: { (data: [ProductSales]) in
                XCTAssertEqual(data.count, expectedData.count)
            }
        waitForExpectations(timeout: 2)
        anyCancellable.cancel()
    }
    
    func testErrorResponse() {
        let request: MockRequest<[ProductSales]> = MockRequest(.failure(.unauthorized))
        let expectation = expectation(description: "wait for request to finish")
        let anyCancellable = request
            .makeRequest(api: .sales(accountId: 123, projectId: 123))
            .receive(on: DispatchQueue.main)
            .sink { result in
                switch result {
                case .failure(let reason):
                    XCTAssertEqual(reason, .unauthorized)
                case .finished: break
                }
                expectation.fulfill()
            } receiveValue: { (data: [ProductSales]) in
                XCTFail("Success was not expect")
            }
        waitForExpectations(timeout: 2)
        anyCancellable.cancel()
    }

}
