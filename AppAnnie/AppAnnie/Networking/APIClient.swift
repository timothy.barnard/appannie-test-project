//
//  APIService.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import Foundation
import Combine
import os

protocol Request {
    func makeRequest<T: Decodable>(api: API) -> AnyPublisher<T, APIFailureReason>
}

/// Possible API failure reason
enum APIFailureReason: Error {
    /// The URLRequest object failed to build
    case invalidRequest
    /// The user is unauthorised
    case unauthorized
    /// The call made is forbidden
    case forbidden
    /// An unknown error has occurred
    case unknownError
    /// A JSON decoding error occurred
    case decodingFailed
}

/// API networking service layer
class APIClient: Request {
    
    private let session: URLSession
    private let decoder: JSONDecoder
    
    /// Constructor
    /// - Parameter session: `URLSession`
    init(_ session: URLSession = .shared, decoder: JSONDecoder) {
        self.session = session
        self.decoder = decoder
    }
    
    func makeRequest<T: Decodable>(api: API) -> AnyPublisher<T, APIFailureReason> {
        guard let request = api.urlRequest else {
            os_log("Invalid URLRequest", log: .networking, type: .error)
            return Fail(error: APIFailureReason.invalidRequest)
            .eraseToAnyPublisher()
        }
        return session.dataTaskPublisher(for: request)
            .tryMap({ response -> (data: Data, response: URLResponse) in
                guard let httpResponse = response.response as? HTTPURLResponse else {
                  throw APIFailureReason.unknownError
                }
                os_log("URL response status code: %d", log: .networking, type: .debug, httpResponse.statusCode)
                if httpResponse.statusCode == 401 {
                  throw APIFailureReason.unauthorized
                }

                if httpResponse.statusCode == 403 {
                    throw APIFailureReason.forbidden
                }

                return response
              })
            .map(\.data)
            .decode(type: T.self, decoder: self.decoder)
            .mapError({ error in
                switch error {
                case is Swift.DecodingError:
                    os_log("Decoding error: %@", log: .networking, type: .error, error.localizedDescription)
                  return .decodingFailed
                case let reason as APIFailureReason:
                    return reason
                default:
                    return .unknownError
                }
              })
            .eraseToAnyPublisher()
    }
}
