//
//  FilterDataTests.swift
//  AppAnnieTests
//
//  Created by Timothy Barnard on 11/09/2021.
//

import XCTest
@testable import AppAnnie

class FilterDataTests: XCTestCase {

    func testClearingFilter() {
        var filterData = FilterData()
        filterData.startDate = Date()
        filterData.endDate = Date()
        filterData.groupByDate = true
        filterData.groupByCountry = true
        filterData.resets()
        XCTAssertNil(filterData.startDate)
        XCTAssertNil(filterData.endDate)
        XCTAssertFalse(filterData.groupByCountry)
        XCTAssertFalse(filterData.groupByDate)
    }

}
