//
//  MockRequest.swift
//  AppAnnieTests
//
//  Created by Timothy Barnard on 11/09/2021.
//

import Foundation
import Combine
@testable import AppAnnie

/// Mocking the request response
class MockRequest<T>: Request {
    typealias OutputType = T
    
    private let result: Result<OutputType, APIFailureReason>
    
    /// Constructor to provide fake data
    /// - Parameters:
    ///   - result: `Result<Decodable, APIFailureReason>` the expect result
    init(_ result: Result<OutputType, APIFailureReason>!) {
        self.result = result
    }
    
    func makeRequest<T>(api: API) -> AnyPublisher<T, APIFailureReason> {
        switch result {
        case .success(let data):
            return Just(data as! T)
                .setFailureType(to: APIFailureReason.self)
                .eraseToAnyPublisher()
        case .failure(let reason):
            return Fail(error: reason)
                .eraseToAnyPublisher()
        }
    }
}
