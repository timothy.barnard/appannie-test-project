//
//  AppAnnieApp.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import SwiftUI

@main
struct AppAnnieApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
