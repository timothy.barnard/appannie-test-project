//
//  OSLog+ext.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import Foundation
import os

extension OSLog {
    
    static var networking = OSLog(subsystem: "com.barnard.AppAnnie", category: "Networking")
}
