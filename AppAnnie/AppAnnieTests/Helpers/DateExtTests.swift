//
//  DateExtTests.swift
//  AppAnnieTests
//
//  Created by Timothy Barnard on 11/09/2021.
//

import XCTest

class DateExtTests: XCTestCase {

    func testRemove() {
        let day1: Date = .create(year: 2021, month: 1, day: 10).remove(days: 5)!
        XCTAssertEqual(day1, .create(year: 2021, month: 1, day: 5))
        
        let day2: Date = .create(year: 2021, month: 1, day: 10).remove(months: 2)!
        XCTAssertEqual(day2, .create(year: 2020, month: 11, day: 10))
        
        let day3: Date = .create(year: 2021, month: 1, day: 10).remove(years: 2)!
        XCTAssertEqual(day3, .create(year: 2019, month: 1, day: 10))
    }

}
