//
//  FilterView.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import SwiftUI

struct FilterView: View {
    @Binding var isPresented: Bool
    @Binding var filterData: FilterData
    
    var closure: () -> Void
    
    var body: some View {
        HStack {
            Button("clear") {
                self.filterData.resets()
            }
                .foregroundColor(.red)
                .padding()
            Spacer()
            Text("filter")
                .font(.largeTitle)
                .bold()
            Spacer()
            Button("done") {
                self.closure()
                self.isPresented.toggle()
            }
                .foregroundColor(.blue)
                .padding()
        }
        Form {
            Section(header: Text("group_by")) {
                Toggle("country", isOn: $filterData.groupByCountry)
                Toggle("date", isOn: $filterData.groupByDate)
            }
            Section(header: Text("date_range")) {
                DatePicker(
                    "start_date",
                    selection: Binding<Date>(
                        get: {self.filterData.startDate ?? Date()},
                        set: {self.filterData.startDate = $0}),
                    displayedComponents: .date)
                TextWithLabel(
                    text: self.filterData.startDateString,
                    labelText: "chosen_start_date")
                DatePicker(
                    "end_date",
                    selection: Binding<Date>(
                        get: {self.filterData.endDate ?? Date()},
                        set: {self.filterData.endDate = $0}),
                    displayedComponents: .date)
                TextWithLabel(
                    text: self.filterData.endDateString,
                    labelText: "chosen_end_date")
            }
        }
    }
}

struct FilterView_Previews: PreviewProvider {
    static var previews: some View {
        FilterView(isPresented: .constant(false), filterData: .constant(.init())) {
            //Closure has been called
        }
    }
}
