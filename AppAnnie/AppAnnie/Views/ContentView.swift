//
//  ContentView.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import SwiftUI
import Combine

struct ContentView: View {
    
    @ObservedObject var viewModel: ProductSalesViewModel = .init()
    @State var showingFilterView: Bool = false
    @State var filterData: FilterData = .init()
    
    @ViewBuilder
    var viewBody: some View {
        switch viewModel.state {
        case .noData:
            Text("no_results_found")
                .bold()
                .font(.largeTitle)
                .multilineTextAlignment(.center)
        case .loading:
            ProgressView()
                .scaleEffect(x: 3, y: 3, anchor: .center)
                .progressViewStyle(CircularProgressViewStyle(tint: Color.blue))
        case .data:
            List(viewModel.productSales) { productSale in
                ProductSaleRowView(productSale: productSale)
            }   
        case .failed(let message):
            Text(message)
                .bold()
                .font(.largeTitle)
                .multilineTextAlignment(.center)
                .padding(10)
            Button("try_again") {
                viewModel.fetchSales(filterData)
            }
        }
    }
    
    var body: some View {
        NavigationView {
            viewBody
            .onAppear {
                viewModel.fetchSales(filterData)
            }
            .navigationBarTitle("Merge 2048")
            .navigationBarItems(
                leading:
                Text("revenue_and_sales")
                    .font(.body)
                    .foregroundColor(Color(.systemGray)),
                trailing:
                    HStack {
                        Button(action: {
                            self.showingFilterView.toggle()
                        }) {
                            Text("filter")
                                .foregroundColor(Color(.darkText))
                        }
                        Image("product")
                            .resizable()
                            .frame(width: 40, height: 40)
                            .clipShape(Circle()) // Clip the image to a circle
                    })
        }
        .sheet(isPresented: $showingFilterView) {
            FilterView(isPresented: $showingFilterView, filterData: $filterData) {
                viewModel.fetchSales(filterData)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
