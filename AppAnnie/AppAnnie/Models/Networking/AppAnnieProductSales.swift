//
//  ProductSales.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import Foundation

/// AppAnnie Product Sales data model
struct AppAnnieProductSales: Decodable {
    let salesList: [AppAnnieSale]
    let nextPage: Int?
    let code: Int
    let prevPage: Int?
    let pageNum: Int
    let currency: String
    let pageIndex: Int
    let market: String
}

/// AppAnnie Sale data model
struct AppAnnieSale: Decodable {
    let date: String //this can be all or a date `2014-03-27`
    let country: String
    let units: AppAnnieUnits
    let revenue: AppAnnieRevenue
}

/// AppAnnie Unit data model
struct AppAnnieUnits: Decodable {
    let product: AppAnnieUnitsProduct
}

/// AppAnnie Revenue data model
struct AppAnnieRevenue: Decodable {
    let product: AppAnnieRevenueProduct
}

/// AppAnnie Units Product data model
struct AppAnnieUnitsProduct: Decodable {
    let promotions: Int
    let downloads: Int
    let updates: Int
    let refunds: Int
}

/// AppAnnie Revenue Product data model
struct AppAnnieRevenueProduct: Decodable {
    let promotions: String
    let downloads: String
    let updates: String
    let refunds: String
}
