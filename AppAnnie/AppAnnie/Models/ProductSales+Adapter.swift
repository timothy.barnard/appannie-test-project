//
//  ProductSales+adapter.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import Foundation

/// Business product sales models
struct ProductSales: Identifiable {
    let id: UUID = .init()
    //MARK: Private properties
    private let downloads: Int
    private let currency: String
    private let revenue: String
    private let date: String
    
    //MARK: - Internal
    let country: String
    
    //Made of of current + revenue
    var sales: String {
        return "\(currency) \(revenue)"
    }
    
    var dateString: String {
        if date == "all" {return ""} //don't show the date if not a real date
        return date
    }
    
    /// Number formatted download value
    var downloadCount: String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.locale = .current
        return numberFormatter.string(from: NSNumber(value: downloads)) ?? "--"
    }
    
    //Constructor
    init(downloads: Int, revenue: String,
         currency: String, country: String,
         date: String) {
        self.downloads = downloads
        self.revenue = revenue
        self.currency = currency
        self.country = country
        self.date = date
    }
}
