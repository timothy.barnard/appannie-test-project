//
//  ModelsDecodingTests.swift
//  AppAnnieTests
//
//  Created by Timothy Barnard on 11/09/2021.
//

import XCTest
@testable import AppAnnie

class ModelsDecodingTests: XCTestCase {
    
    var productSales: AppAnnieProductSales?

    override func setUp() {
        super.setUp()
        guard let url = Bundle(for: type(of: self)).url(forResource: "fake_data", withExtension: "json") else {
            XCTFail("Unable to path for sample_mrz_request_body")
            return
        }
        let data: Data? = try? Data(contentsOf: url)
        XCTAssertNotNil(data)
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            self.productSales = try decoder.decode(AppAnnieProductSales.self, from: data!)
        } catch {
            print(error)
        }
        XCTAssertNotNil(self.productSales)
        continueAfterFailure = false
    }
    
    override func tearDown() {
        super.tearDown()
        self.productSales = nil
    }
    
    func testProductSales() {
        XCTAssertEqual(productSales?.currency, "USD")
        XCTAssertEqual(productSales?.code, 200)
        XCTAssertEqual(productSales?.market, "ios")
        XCTAssertEqual(productSales?.pageNum, 1)
    }
    
    func testSalesContents() {
        XCTAssertEqual(productSales?.salesList.count, 4)
        
        let firstSale = productSales!.salesList.first!
        XCTAssertEqual(firstSale.country, "AE")
        XCTAssertEqual(firstSale.units.product.downloads, 23)
        
        let saleARCountry = productSales!.salesList.first(where: {$0.country == "AR"})!
        XCTAssertEqual(saleARCountry.revenue.product.downloads, "20.00")
        
    }
}
