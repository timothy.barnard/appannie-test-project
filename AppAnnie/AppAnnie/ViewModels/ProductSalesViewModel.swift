//
//  ProductSalesViewModel.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import Foundation
import Combine
import SwiftUI

class ProductSalesViewModel: ObservableObject {
    
    //MARK: - Private properties
    private let service: ProductSalesServices
    private var cancellable: AnyCancellable?
    
    /// The available states for the view
    enum States: Equatable {
        /// Waiting for the fetch sales to finish
        case loading
        /// Fetch sales has finished with data
        case data
        /// Fetch sales has finished with no data
        case noData
        /// Fetch sales has finished with an error
        case failed(LocalizedStringKey)
    }
    
    @Published var productSales: [ProductSales] = []
    @Published var state: States = .loading
    
    init(_ service: ProductSalesServices = .init()) {
        self.service = service
    }
    
    /// Fetch sales for the product with the given filter
    /// - Parameter filterData: `FilterData`
    func fetchSales(_ filterData: FilterData) {
        self.state = .loading
        cancellable = self.service.getSales(filterData)
            .receive(on: DispatchQueue.main)
            .sink { result in
                switch result {
                case .failure(let reason):
                    switch reason {
                    case .unauthorized:
                        self.state = .failed("unauthorized_message")
                    default:
                        self.state = .failed("error_message")
                    }
                case .finished: break
                }
            } receiveValue: { sales in
                self.productSales = sales
                self.state = sales.count == 0 ? .noData : .data
            }

    }
}
