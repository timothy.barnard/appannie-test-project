//
//  Date+ext.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 12/09/2021.
//

import Foundation

extension Date {
    
    /// Medium date style without time string format
    var toMediumDateSyleString: String {
        let dateFormatter: DateFormatter = .init()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter.string(from: self)
    }
}
