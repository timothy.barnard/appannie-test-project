//
//  AppAnnieCountry.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 12/09/2021.
//

import Foundation

//AppAnnie Country list data model
struct AppAnnieCountryList: Decodable {
    let countryList: [AppAnnieCountry]
}

//AppAnnie Country data model
struct AppAnnieCountry: Decodable {
    let countryName: String
    let countryCode: String
}
