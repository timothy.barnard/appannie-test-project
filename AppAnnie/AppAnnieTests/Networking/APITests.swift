//
//  APITests.swift
//  AppAnnieTests
//
//  Created by Timothy Barnard on 11/09/2021.
//

import XCTest
@testable import AppAnnie

class APITests: XCTestCase {
    
    func testDateFilter() {
        let start: Date = .create(year: 2021, month: 1, day: 1)
        let end: Date = .create(year: 2021, month: 12, day: 31)
        let dateFilter = API.DateFilter(start: start, end: end)
        let queryItems = dateFilter.queryItems
        XCTAssertEqual(queryItems[0].name, "start_date")
        XCTAssertEqual(queryItems[1].name, "end_date")
        
        XCTAssertEqual(queryItems[0].value, "2021-01-01")
        XCTAssertEqual(queryItems[1].value, "2021-12-31")
    }

    func testSalesURL() {
        let api1: API = .sales(accountId: 123, projectId: 123, breakDown: nil, dateFilter: nil)
        XCTAssertEqual(api1.urlRequest?.url, URL(string: "https://api.appannie.com/v1.3/accounts/123/products/123/sales?")!)
        
        let api2: API = .sales(accountId: 123, projectId: 123, breakDown: .country, dateFilter: nil)
        XCTAssertEqual(api2.urlRequest?.url, URL(string: "https://api.appannie.com/v1.3/accounts/123/products/123/sales?break_down=country")!)
        
        
        let startDate: Date = .create(year: 2020, month: 1, day: 1)
        let endDate: Date = .create(year: 2021, month: 12, day: 31)
        let dateFilter: API.DateFilter = .init(start: startDate, end: endDate)
        let api3: API = .sales(accountId: 123, projectId: 123, breakDown: .country, dateFilter: dateFilter)
        XCTAssertEqual(api3.urlRequest?.url, URL(string: "https://api.appannie.com/v1.3/accounts/123/products/123/sales?break_down=country&start_date=2020-01-01&end_date=2021-12-31")!)
    }
    
    func testCountiesURL() {
        let api1: API = .countries
        XCTAssertEqual(api1.urlRequest?.url, URL(string: "https://api.appannie.com/v1.3/meta/countries")!)
    }
    
    func testAPITokenExists() {
        let request = API.sales(accountId: 123, projectId: 123, breakDown: nil, dateFilter: nil).urlRequest
        let headers = request!.allHTTPHeaderFields!
        let token = headers["Authorization"]
        //meaning the token was not retrieved from the Info.plist
        XCTAssertNotEqual(token, "Bearer ")
    }

}
