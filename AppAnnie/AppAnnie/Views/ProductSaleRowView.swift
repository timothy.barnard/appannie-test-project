//
//  ProductSaleView.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import SwiftUI

struct ProductSaleRowView: View {
    
    var productSale: ProductSales
    
    var body: some View {
        HStack {
            VStack {
                VStack(alignment: .leading) {
                    Text(productSale.country)
                        .padding(.bottom, 5)
                    Text(productSale.dateString)
                }
            }
            Spacer()
            VStack(alignment: .leading) {
                TextWithLabel(
                    text: productSale.sales,
                    labelText: "revenue")
                TextWithLabel(
                    text: productSale.downloadCount,
                    labelText: "downloads")
            }
        }
    }
}

struct ProductSaleRowView_Previews: PreviewProvider {
    static var previews: some View {
        ProductSaleRowView(productSale: .init(downloads: 1, revenue: "1.00", currency: "$", country: "all", date: "all"))
    }
}
