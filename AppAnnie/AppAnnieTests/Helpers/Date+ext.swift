//
//  Date+ext.swift
//  AppAnnieTests
//
//  Created by Timothy Barnard on 11/09/2021.
//

import Foundation

/// Helper extension for easier testing
extension Date {
    
    static func create(year: Int, month: Int, day: Int) -> Date {
        // Specify date components
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.timeZone = .current
        dateComponents.hour = 13
        dateComponents.minute = 0

        let userCalendar = Calendar(identifier: .gregorian)
        return userCalendar.date(from: dateComponents)!
    }
    
    /// Remove the provided months from the current Date
    func remove(months: Int) -> Date? {
        Calendar.current.date(byAdding: .month, value: -(months), to: self)
    }
    
    /// Remove the provided days from the current Date
    func remove(days: Int) -> Date? {
        Calendar.current.date(byAdding: .day, value: -(days), to: self)
    }
    
    /// Remove the provided years from the current Date
    func remove(years: Int) -> Date? {
        Calendar.current.date(byAdding: .year, value: -(years), to: self)
    }
}
