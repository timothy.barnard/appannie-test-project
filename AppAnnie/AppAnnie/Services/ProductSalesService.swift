//
//  ProductSalesService.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import Foundation
import Combine

/**
 Product sales service layer for retrieving the data
 and parsing it into models that the ModelView layer can use.
 */
class ProductSalesServices {
    
    private let request: Request
    
    init(_ request: Request) {
        self.request = request
    }
    
    init() {
        let jsonDecoder: JSONDecoder = .init()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        self.request = APIClient(.shared, decoder: jsonDecoder)
    }
    
    /// Get list of sales for a given product
    /// - Parameter productId: `Int`
    /// - Returns: `AnyPublisher<[ProductSales], APIFailureReason>`
    func getSales(_ filterData: FilterData) -> AnyPublisher<[ProductSales], APIFailureReason> {
        let api: API = filterData.toAPI
        let countryAPI: API = .countries
        let countryPublisher: AnyPublisher<AppAnnieCountryList, APIFailureReason> =
            request.makeRequest(api: countryAPI)
        let salesPublisher: AnyPublisher<AppAnnieProductSales, APIFailureReason> =
            request.makeRequest(api: api)
        return Publishers.CombineLatest(salesPublisher, countryPublisher)
            .map { (productSales: AppAnnieProductSales, countryList: AppAnnieCountryList) -> [ProductSales] in
                productSales.salesList.adaptToProductSales("$", countryList: countryList)
            }
            .eraseToAnyPublisher()
    }
}

fileprivate extension Array where Element == AppAnnieSale {
    
    /// DescriptionConverts `[AppAnnieSale]` to `[ProductSales]`
    /// - Parameters:
    ///   - currency: `String` the current filtered currency
    ///   - countryList: `AppAnnieCountryList` list of countries to get the readable format
    /// - Returns: `[ProductSales]`
    func adaptToProductSales(_ currency: String, countryList: AppAnnieCountryList) -> [ProductSales] {
        return self.compactMap { appAnnieSale in
            let downloads = appAnnieSale.units.product.downloads
            let revenue = appAnnieSale.revenue.product.downloads
            let date = appAnnieSale.date
            var country = countryList.countryList.first(where: { $0.countryCode == appAnnieSale.country })?.countryName ?? appAnnieSale.country
            if country == "all" {
                country = "Global" //quick change to explain what all means
            }
            return .init(downloads: downloads, revenue: revenue, currency: currency, country: country, date: date)
        }
    }
}

fileprivate extension FilterData {
    
    /// Helper property to convert the filtered data into an API
    var toAPI: API {
        var dateFilter: API.DateFilter? = nil
        if let start = self.startDate, let end = self.endDate {
            dateFilter = .init(start: start, end: end)
        }
        let breakDown: API.BreakDown? = .init(
            groupByCountry, groupByDate: groupByDate)
        
        return .sales(
            accountId: self.accountId,
            projectId: self.productId, breakDown: breakDown, dateFilter: dateFilter)
    }
}

fileprivate extension API.BreakDown {
    
    /// Helper constructor to convert group by booleans into `API.Breakdown`
    /// - Parameters:
    ///   - groupByCountry: `Bool`
    ///   - groupByDate: `Bool`
    init?(_ groupByCountry: Bool, groupByDate: Bool) {
        switch (groupByCountry, groupByDate) {
        case (false, false): return nil
        case (false, true): self = .date
        case (true, false): self = .country
        case (true, true): self = .countryAndDay
        }
    }
}
