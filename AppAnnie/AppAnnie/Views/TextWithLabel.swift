//
//  TextWithLabel.swift
//  AppAnnie
//
//  Created by Timothy Barnard on 11/09/2021.
//

import SwiftUI

/// Text view that includes helper text label above
struct TextWithLabel: View {

    /// The main text
    var text: String
    
    /// The subtitle label text
    var labelText: LocalizedStringKey
    
    var body: some View {
        HStack {
            Text(labelText)
                .foregroundColor(Color(.systemGray))
            Text(text)
        }
    }
}

struct TextWithLabel_Previews: PreviewProvider {
    static var previews: some View {
        TextWithLabel(text: "Something", labelText: "Helper text")
    }
}
